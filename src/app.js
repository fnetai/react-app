import React, { StrictMode } from 'react';
import ReactDOMClient from "react-dom/client";
import { Provider, Store, Action } from "@fnet/react-app-state";

import Host from "./host";

export default class {
    #container;
    #formVersion;
    #form;
    #formProps;

    #backgroundImage;

    #backgroundColor;

    #headElements = [];

    async init(options) {
        if (this.#container) return; // Already initialized

        this.#container = options.container;
        this.#formVersion = 0;

        await this.initFont();
        await this.initFontIcons();
        await this.renderApp();
    }

    async initFont() {
        const id = 'flownet-react-app-font';
        if (document.getElementById(id)) return; // Already initialized

        const font = document.createElement('link');
        font.href = 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap';
        font.rel = 'stylesheet';
        font.id = id;
        document.head.appendChild(font);
        this.#headElements.push(font);
    }

    async initFontIcons() {
        const id = 'flownet-react-app-font'; // Changed the ID since it was a duplicate
        if (document.getElementById(id)) return; // Already initialized

        const font = document.createElement('link');
        font.href = 'https://fonts.googleapis.com/icon?family=Material+Icons';
        font.rel = 'stylesheet';
        font.id = id;
        document.head.appendChild(font);
        this.#headElements.push(font);
    }

    async renderApp() {
        const root = ReactDOMClient.createRoot(this.#container);
        root.render(
            <StrictMode>
                <Provider>
                    <Host app={this} />
                </Provider>
            </StrictMode>
        );
    }

    async setActiveForm(context) {
        this.#form = context.form;
        this.#formProps = context.props;
        Store.dispatch(Action.update('formVersion', ++this.#formVersion));
    }

    get activeForm() {
        return this.#form;
    }

    get activeFormProps() {
        return this.#formProps;
    }

    async setBackgroundImage(context) {
        this.#backgroundImage = context.value;
        Store.dispatch(Action.update('formVersion', ++this.#formVersion));
    }

    getBackgroundImage() {
        return this.#backgroundImage;
    }

    async setBackgroundColor(context) {
        this.#backgroundColor = context.value;
        Store.dispatch(Action.update('formVersion', ++this.#formVersion));
    }

    getBackgroundColor() {
        return this.#backgroundColor;
    }

    async destroy() {
        this.#headElements.forEach(element => {
            document.head.removeChild(element);
        });
        this.#headElements = [];
        this.#container = null;
        this.#form = null;
        this.#formProps = null;
        this.#backgroundImage = null;
        this.#backgroundColor = null;
    }
}