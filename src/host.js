import React from 'react';
import { useSelector } from "@fnet/react-app-state";

export default ({ app }) => {
  const formVersion = useSelector(state => state.default.formVersion);

  const ActiveForm = formVersion ? app.activeForm : React.Fragment;
  const backgroundImage = app.getBackgroundImage();
  const backgroundColor = app.getBackgroundColor();

  const style = {
    height: '100%',
    ...(backgroundImage ? { backgroundImage: `url(${backgroundImage})` } : {}),
    ...(backgroundColor ? { backgroundColor } : {}),
  };

  return (
    <div style={style}>
      <ActiveForm />
    </div>
  );
}